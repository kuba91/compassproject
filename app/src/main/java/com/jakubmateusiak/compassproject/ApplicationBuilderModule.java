package com.jakubmateusiak.compassproject;

import com.jakubmateusiak.compassproject.features.compass.CompassFragment;
import com.jakubmateusiak.compassproject.features.compass.di.CompassModule;
import com.jakubmateusiak.compassproject.features.gps.di.GpsModule;
import com.jakubmateusiak.compassproject.qualifiers.PerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Jakub on 03.01.2018.
 */

@Module
public abstract class ApplicationBuilderModule {

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {GpsModule.class, CompassModule.class})
    @PerFragment
    abstract CompassFragment bindCompassFragment();
}
