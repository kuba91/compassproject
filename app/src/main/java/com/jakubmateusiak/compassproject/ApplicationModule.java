package com.jakubmateusiak.compassproject;

import android.content.Context;
import android.hardware.SensorManager;

import com.jakubmateusiak.compassproject.qualifiers.ApplicationContext;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Jakub on 03.01.2018.
 */

@Module
public class ApplicationModule {

    @Provides
    @ApplicationContext
    Context provideAppContet(CompassApplication application){
        return application.getApplicationContext();
    }

    @Provides
    SensorManager provideSensorManager(@ApplicationContext Context context){
        return (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

}
