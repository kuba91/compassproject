package com.jakubmateusiak.compassproject.utils;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.jakubmateusiak.compassproject.R;


/**
 * Created by Jakub on 30.08.2016.
 */
public class NavigationUtils {

    public static void replaceFragmentWithoutBackstack(AppCompatActivity activity, Fragment f) {
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, f, f.getClass().getSimpleName())
                .commit();
    }


}
