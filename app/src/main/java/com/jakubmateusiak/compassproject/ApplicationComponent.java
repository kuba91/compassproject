package com.jakubmateusiak.compassproject;

import android.content.Context;

import com.jakubmateusiak.compassproject.qualifiers.ApplicationContext;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by Jakub on 03.01.2018.
 */

@Component(modules = {AndroidSupportInjectionModule.class, ApplicationBuilderModule.class, ApplicationModule.class})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder application(CompassApplication application);
        ApplicationComponent build();
    }

    @ApplicationContext
    Context context();

    void inject(CompassApplication compassApplication);
}
