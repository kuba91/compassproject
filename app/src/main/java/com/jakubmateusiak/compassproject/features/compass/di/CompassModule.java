package com.jakubmateusiak.compassproject.features.compass.di;

import com.jakubmateusiak.compassproject.features.compass.Compass;
import com.jakubmateusiak.compassproject.features.compass.mvp.CompassPresenterFactory;
import com.jakubmateusiak.compassproject.features.gps.GpsManager;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Jakub on 04.01.2018.
 */

@Module
public class CompassModule {

    @Provides
    CompassPresenterFactory providePresenterFactory(Compass compass, GpsManager gpsManager){
        return new CompassPresenterFactory(gpsManager, compass);
    }
}
