package com.jakubmateusiak.compassproject.features.compass;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.view.WindowManager;

import com.jakubmateusiak.compassproject.qualifiers.ApplicationContext;

import javax.inject.Inject;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by Jakub on 03.01.2018.
 */
//todo change sensor type
public class Compass implements SensorEventListener {

    private Context mContext;
    private SensorManager mSensorManager;
    private PublishSubject<Double> mDestinationDirectionSubject;
    private PublishSubject<Double> mNorthDirectionSubject;
    private Location mCurrentPosition;
    private Location mDestination;
    private float[] mGravity;
    private float[] mGeomagnetic;
    private float[] mSmoothed = new float[3];
    private float[] mRotation = new float[9];
    private float[] mOrientation = new float[3];

    @Inject
    public Compass(SensorManager sensorManager, @ApplicationContext Context context) {
        mSensorManager = sensorManager;
        mContext = context;
        mDestinationDirectionSubject = PublishSubject.create();
        mNorthDirectionSubject = PublishSubject.create();
    }

    public void start() {
        Sensor sensorGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor sensorMagnetic = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mSensorManager.registerListener(this, sensorGravity,
                SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, sensorMagnetic,
                SensorManager.SENSOR_DELAY_GAME);
    }

    public void stop() {
        mSensorManager.unregisterListener(this);
    }

    public void setCurrentPosition(Location currentPosition) {
        mCurrentPosition = currentPosition;
    }

    public void setDestination(Location destination) {
        mDestination = destination;
    }

    private double getBearingBetweenCurrentPositionAnddestination() {
        return mCurrentPosition.bearingTo(mDestination);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        double bearing = -1;
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (mGravity == null)
                mGravity = new float[3];
            mSmoothed = lowPass(event.values, mGravity);
            mGravity[0] = mSmoothed[0];
            mGravity[1] = mSmoothed[1];
            mGravity[2] = mSmoothed[2];

        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            if (mGeomagnetic == null)
                mGeomagnetic = new float[3];
            mSmoothed = lowPass(event.values, mGeomagnetic);
            mGeomagnetic[0] = mSmoothed[0];
            mGeomagnetic[1] = mSmoothed[1];
            mGeomagnetic[2] = mSmoothed[2];
        }
        if (mGravity != null && mGeomagnetic != null) {
            SensorManager.getRotationMatrix(mRotation, null, mGravity, mGeomagnetic);

             float[] remapCoords = new float[9];
          //  SensorManager.remapCoordinateSystem(mRotation, SensorManager.AXIS_X, SensorManager.AXIS_Z, remapCoords);
          //  SensorManager.getOrientation(remapCoords, mOrientation);

            int rotation = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
            if(rotation == 0)
                SensorManager.remapCoordinateSystem(mRotation, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_Y, remapCoords);
            else
                SensorManager.remapCoordinateSystem(mRotation, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, remapCoords);
            SensorManager.getOrientation(remapCoords, mOrientation);
          //  SensorManager.getOrientation(mRotation, mOrientation);
            bearing = mOrientation[0];
            bearing = Math.toDegrees(bearing);
            if (bearing < 0) {
                bearing += 360;
            }
            mNorthDirectionSubject.onNext(-bearing);


        }
        if (bearing != -1 && mCurrentPosition != null && mDestination != null) {
            double degreesToPoint = getBearingBetweenCurrentPositionAnddestination() - bearing;
            mDestinationDirectionSubject.onNext(degreesToPoint);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private float[] lowPass(float[] input, float[] output) {
        if (output == null) return input;
        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + 0.25f * (input[i] - output[i]);
        }
        return output;
    }

    public PublishSubject<Double> getDestinationDirectionSubject() {
        return mDestinationDirectionSubject;
    }

    public PublishSubject<Double> getNorthDirectionSubject() {
        return mNorthDirectionSubject;
    }
}
