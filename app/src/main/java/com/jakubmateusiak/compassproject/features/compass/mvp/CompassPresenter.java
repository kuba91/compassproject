package com.jakubmateusiak.compassproject.features.compass.mvp;

import android.arch.lifecycle.ViewModel;
import android.location.Location;
import android.location.LocationManager;

import com.jakubmateusiak.compassproject.features.compass.Compass;
import com.jakubmateusiak.compassproject.features.gps.GpsManager;
import com.jakubmateusiak.compassproject.utils.JavaTextUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Jakub on 04.01.2018.
 */

public class CompassPresenter extends ViewModel implements CompassMVP.Presenter {

    private Compass mCompass;
    private GpsManager mGpsManager;
    private CompassMVP.View mView;
    private CompositeDisposable mCompositeDisposable;
    private Location mCurrentPosition;
    private Location mDestination;

    public CompassPresenter(Compass compass, GpsManager gpsManager) {
        mCompass = compass;
        mGpsManager = gpsManager;
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attach(CompassMVP.View view) {
        mView = view;
        mCompositeDisposable.clear();
        if(mDestination != null){
            setDestinationPoint(String.valueOf(mDestination.getLatitude()), String.valueOf(mDestination.getLongitude()));
        }
        mCompass.start();
        mCompositeDisposable.add(
                mCompass.getNorthDirectionSubject()
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(aDouble -> {
                            mView.setDirectionToNorth(aDouble);
                            // mView.setDegree(aDouble);
                        })
        );
    }

    @Override
    public void detach() {
        mCompositeDisposable.clear();
        mGpsManager.stop();
        mCompass.stop();
        mView = null;

    }

    @Override
    public void setDestinationPoint(String lat, String lng) {
        if (!mGpsManager.isGPSEnabled()) {
            mView.handleError(CompassMVP.ERR_GPS_DISABLED);
        }else
        if (!validate(lat, lng)) {
            double latitude = Double.parseDouble(lat);
            double longitude = Double.parseDouble(lng);
            mDestination = new Location(LocationManager.PASSIVE_PROVIDER);
            mDestination.setLatitude(latitude);
            mDestination.setLongitude(longitude);
            mView.setDestinationLat(lat);
            mView.setDestinationLng(lng);
            mCompass.setDestination(mDestination);

            mCompositeDisposable.add(
                    mGpsManager.start().subscribe(locationResult -> {

                        mCurrentPosition = locationResult.getLastLocation();
                        mCompass.setCurrentPosition(mCurrentPosition);
                    })
            );
            mCompositeDisposable.add(
                    mCompass.getDestinationDirectionSubject()
                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe(aDouble -> {
                                mView.setDegree(aDouble);
                            })
            );

            mView.showNavigationElements();
        }
    }

    private boolean validate(String lat, String lng) {
        boolean haveError = false;
        if (JavaTextUtils.isEmpty(lat)) {
            haveError = true;
            mView.handleError(CompassMVP.ERR_LAT_EMPTY);
        } else {
            double latitude = Double.parseDouble(lat);
            if (latitude < -90 || latitude > 90) {
                haveError = true;
                mView.handleError(CompassMVP.ERR_LAT_RANGE_ERROR);
            }
        }
        if (JavaTextUtils.isEmpty(lng)) {
            haveError = true;
            mView.handleError(CompassMVP.ERR_LNG_EMPTY);
        } else {
            double longitude = Double.parseDouble(lng);
            if (longitude < -180 || longitude > 180) {
                haveError = true;
                mView.handleError(CompassMVP.ERR_LNG_RANGE_ERROR);
            }
        }
        return haveError;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        detach();
    }

    @Override
    public void stopNavigation() {
        mView.hideNavigationElements();
        mView.setDestinationLat("-");
        mView.setDestinationLng("-");
        mCompass.stop();
        mGpsManager.stop();
        mDestination = null;
        mCompass.setDestination(null);
        mCompositeDisposable.clear();
    }
}
