package com.jakubmateusiak.compassproject.features.compass.mvp;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.jakubmateusiak.compassproject.features.compass.Compass;
import com.jakubmateusiak.compassproject.features.gps.GpsManager;

/**
 * Created by Jakub on 06.01.2018.
 */

public class CompassPresenterFactory implements ViewModelProvider.Factory {

    private GpsManager mGpsManager;
    private Compass mCompass;

    public CompassPresenterFactory(GpsManager gpsManager, Compass compass) {
        mGpsManager = gpsManager;
        mCompass = compass;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CompassPresenter(mCompass, mGpsManager);
    }
}
