package com.jakubmateusiak.compassproject.features.compass.mvp;

/**
 * Created by Jakub on 04.01.2018.
 */

public interface CompassMVP {

    int ERR_LAT_EMPTY = 0;
    int ERR_LNG_EMPTY = 1;
    int ERR_LAT_RANGE_ERROR = 2;
    int ERR_LNG_RANGE_ERROR = 3;
    int ERR_GPS_DISABLED = 4;

    interface View{
        void setDegree(double degree);
        void setDirectionToNorth(double direction);
        void setDestinationLat(String lat);
        void setDestinationLng(String lng);
        void handleError(int errorCode);
        void showNavigationElements();
        void hideNavigationElements();
    }

    interface Presenter{
        void attach(View view);
        void detach();
        void setDestinationPoint(String lat, String lng);
        void stopNavigation();
    }
}
