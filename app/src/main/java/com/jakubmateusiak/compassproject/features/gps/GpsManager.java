package com.jakubmateusiak.compassproject.features.gps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.LocationManager;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.jakubmateusiak.compassproject.qualifiers.ApplicationContext;
import com.jakubmateusiak.compassproject.qualifiers.PerFragment;

import javax.inject.Inject;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by Jakub on 03.01.2018.
 */

@PerFragment
public class GpsManager {
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private PublishSubject<LocationResult> mLocationResultPublishSubject;
    private Context mContext;

    @Inject
    public GpsManager(FusedLocationProviderClient fusedLocationProviderClient,
                      LocationCallback locationCallback,
                      LocationRequest locationRequest,
                      PublishSubject<LocationResult> locationResultPublishSubject,
                      @ApplicationContext Context context){
        mFusedLocationProviderClient = fusedLocationProviderClient;
        mLocationCallback = locationCallback;
        mLocationRequest = locationRequest;
        mLocationResultPublishSubject = locationResultPublishSubject;
        mContext = context;
    }

    @SuppressLint("MissingPermission")
    public PublishSubject<LocationResult> start(){
        stop();
        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        return mLocationResultPublishSubject;
    }

    public boolean isGPSEnabled(){
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager == null || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void stop(){
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }
}
