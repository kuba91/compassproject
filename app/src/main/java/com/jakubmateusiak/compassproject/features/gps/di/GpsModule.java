package com.jakubmateusiak.compassproject.features.gps.di;

import android.content.Context;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.jakubmateusiak.compassproject.qualifiers.ApplicationContext;
import com.jakubmateusiak.compassproject.qualifiers.PerFragment;

import dagger.Module;
import dagger.Provides;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Jakub on 03.01.2018.
 */

@Module
public class GpsModule {

    @Provides
    public LocationRequest provideLocationRequest(){
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    @Provides
    public FusedLocationProviderClient provideFusedLocationProviderClient(@ApplicationContext Context context){
        return new FusedLocationProviderClient(context);
    }

    @Provides
    public LocationCallback provideLocationCallback(PublishSubject<LocationResult> mLocationResultPublishSubject){
        return new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mLocationResultPublishSubject.onNext(locationResult);
            }
        };
    }

    @Provides
    @PerFragment
    public PublishSubject<LocationResult> provideLocationResult(){
        return PublishSubject.create();
    }
}
