package com.jakubmateusiak.compassproject.features.compass;


import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakubmateusiak.compassproject.R;
import com.jakubmateusiak.compassproject.features.compass.mvp.CompassMVP;
import com.jakubmateusiak.compassproject.features.compass.mvp.CompassPresenter;
import com.jakubmateusiak.compassproject.features.compass.mvp.CompassPresenterFactory;
import com.jakubmateusiak.compassproject.features.gps.GpsManager;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompassFragment extends Fragment implements CompassMVP.View {

    @BindView(R.id.current_latitude_tv)
    TextView mCurrentLatitudeTv;
    @BindView(R.id.current_longitude_tv)
    TextView mCurrentLongitudeTv;
    @BindView(R.id.destination_latitude_et)
    EditText mDestinationLatitudeEt;
    @BindView(R.id.destination_longitude_et)
    EditText mDestinationLongitudeEt;
    @BindView(R.id.compass_iv)
    ImageView mCompassIv;
    @BindView(R.id.destination_arrow_iv)
    ImageView mDestinationArrowIv;
    @BindView(R.id.stop_navigate_btn)
    Button mStopNavigateBtn;
    Unbinder unbinder;

    @Inject
    GpsManager mGpsManager;
    @Inject
    CompassPresenterFactory mCompassPresenterFactory;

    CompassMVP.Presenter mPresenter;


    @OnTextChanged(R.id.destination_latitude_et)
    public void destinationLatETChanged() {
        mDestinationLatitudeEt.setError(null);
    }

    @OnTextChanged(R.id.destination_longitude_et)
    public void destinationLngETChanged() {
        mDestinationLongitudeEt.setError(null);
    }

    public static CompassFragment newInstance() {

        Bundle args = new Bundle();

        CompassFragment fragment = new CompassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public CompassFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_compass, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCurrentLatitudeTv.setText(getString(R.string.current_latitude_s, "-"));
        mCurrentLongitudeTv.setText(getString(R.string.current_longitude_s, "-"));
        mPresenter = ViewModelProviders.of(this, mCompassPresenterFactory).get(CompassPresenter.class);
        mPresenter.attach(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.detach();
        unbinder.unbind();
    }

    @OnClick({R.id.navigate_btn, R.id.stop_navigate_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.navigate_btn:
                new RxPermissions(getActivity())
                        .request(Manifest.permission.ACCESS_FINE_LOCATION)
                        .subscribe(granted -> {
                            if (granted) {
                                mPresenter.setDestinationPoint(mDestinationLatitudeEt.getText().toString(), mDestinationLongitudeEt.getText().toString());
                                mGpsManager.start();
                            }
                        });
                break;
            case R.id.stop_navigate_btn:
                mPresenter.stopNavigation();
                break;
        }


    }

    @Override
    public void setDegree(double degree) {
        mDestinationArrowIv.setRotation((float) degree);
    }

    @Override
    public void setDirectionToNorth(double direction) {
        mCompassIv.setRotation((float) direction);

    }

    @Override
    public void setDestinationLat(String lat) {
        mCurrentLatitudeTv.setText(getString(R.string.current_latitude_s, lat));
    }

    @Override
    public void setDestinationLng(String lng) {
        mCurrentLongitudeTv.setText(getString(R.string.current_longitude_s, lng));

    }

    @Override
    public void handleError(int errorCode) {
        switch (errorCode) {
            case CompassMVP.ERR_LAT_EMPTY:
                mDestinationLatitudeEt.setError(getString(R.string.err_lat_empty));
                break;
            case CompassMVP.ERR_LAT_RANGE_ERROR:
                mDestinationLatitudeEt.setError(getString(R.string.err_lat_range));
                break;
            case CompassMVP.ERR_LNG_EMPTY:
                mDestinationLongitudeEt.setError(getString(R.string.err_lng_empty));
                break;
            case CompassMVP.ERR_LNG_RANGE_ERROR:
                mDestinationLongitudeEt.setError(getString(R.string.err_lng_range));
                break;
            case CompassMVP.ERR_GPS_DISABLED:
                showGpsEnableDialog();
            break;
        }
    }

    @Override
    public void showNavigationElements() {
        mDestinationArrowIv.setVisibility(View.VISIBLE);
        mStopNavigateBtn.setEnabled(true);

    }

    @Override
    public void hideNavigationElements() {
        mDestinationArrowIv.setVisibility(View.INVISIBLE);
        mStopNavigateBtn.setEnabled(false);
    }

    private void showGpsEnableDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.location_disabled);
        builder.setMessage(R.string.need_location_turned_on);
        builder.setPositiveButton(R.string.enable_gps, (dialog, which) -> {
            dialog.dismiss();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 12);
        }).setNegativeButton(R.string.cancel, (dialog, which) -> {

        });
        AlertDialog mGPSDialog = builder.create();
        mGPSDialog.setCancelable(false);
        mGPSDialog.show();
    }
}
