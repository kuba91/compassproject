package com.jakubmateusiak.compassproject.features.compass;

import com.jakubmateusiak.compassproject.BaseTest;
import com.jakubmateusiak.compassproject.features.compass.mvp.CompassMVP;
import com.jakubmateusiak.compassproject.features.compass.mvp.CompassPresenter;
import com.jakubmateusiak.compassproject.features.gps.GpsManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import io.reactivex.subjects.PublishSubject;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Created by Jakub on 05.01.2018.
 */
@RunWith(JUnit4.class)
public class CompassPresenterTest extends BaseTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    Compass mCompass;
    @Mock
    GpsManager mGpsManager;
    @Mock
    CompassMVP.View mView;

    private CompassMVP.Presenter mCompassPresenter;

    @Before
    public void setup(){
        when(mCompass.getDestinationDirectionSubject()).thenReturn(PublishSubject.create());
        when(mCompass.getNorthDirectionSubject()).thenReturn(PublishSubject.create());
        when(mGpsManager.isGPSEnabled()).thenReturn(true);
        mCompassPresenter = new CompassPresenter(mCompass, mGpsManager);
        mCompassPresenter.attach(mView);
    }

    @Test
    public void emptyLatLngValidationTest(){
        mCompassPresenter.setDestinationPoint("", "");
        Mockito.verify(mView).handleError(CompassMVP.ERR_LAT_EMPTY);
        Mockito.verify(mView).handleError(CompassMVP.ERR_LNG_EMPTY);
        Mockito.verifyNoMoreInteractions(mView);
    }

    @Test
    public void emptyLatValidationTest(){
        mCompassPresenter.setDestinationPoint("", "15.2");
        Mockito.verify(mView).handleError(CompassMVP.ERR_LAT_EMPTY);
        Mockito.verifyNoMoreInteractions(mView);
    }
    @Test
    public void emptyLngValidationTest(){
        mCompassPresenter.setDestinationPoint("52.1155", "");
        Mockito.verify(mView).handleError(CompassMVP.ERR_LNG_EMPTY);
        Mockito.verifyNoMoreInteractions(mView);
    }

    @Test
    public void rangesValidationTest(){
        doAnswer(invocation -> PublishSubject.create()).when(mGpsManager).start();
        doAnswer(invocation -> PublishSubject.create()).when(mCompass).start();
        mCompassPresenter.setDestinationPoint("-90.1", "-180.1");
        mCompassPresenter.setDestinationPoint("90.1", "180.1");
        mCompassPresenter.setDestinationPoint("90.0", "180.0");

        Mockito.verify(mView, times(2)).handleError(CompassMVP.ERR_LNG_RANGE_ERROR);
        Mockito.verify(mView, times(2)).handleError(CompassMVP.ERR_LAT_RANGE_ERROR);
        Mockito.verify(mView).setDestinationLng("180.0");
        Mockito.verify(mView).setDestinationLat("90.0");
        Mockito.verify(mView).showNavigationElements();
        Mockito.verifyNoMoreInteractions(mView);


    }
}
