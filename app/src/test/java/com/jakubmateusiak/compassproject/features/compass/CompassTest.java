package com.jakubmateusiak.compassproject.features.compass;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.Location;
import android.view.Display;
import android.view.WindowManager;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RobolectricTestRunner;

import java.lang.reflect.Field;

import io.reactivex.observers.TestObserver;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by Jakub on 03.01.2018.
 */
@RunWith(RobolectricTestRunner.class)
public class CompassTest {

    @Mock
    SensorManager mSensorManager;
    @Mock
    Context mContext;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void compassTest() {
        //when
        WindowManager windowManager = Mockito.mock(WindowManager.class);
        when(windowManager.getDefaultDisplay()).thenAnswer(invocation -> {
            Display display = Mockito.mock(Display.class);
            when(display.getRotation()).thenReturn(0);
            return display;
        });

        when(mContext.getSystemService(Context.WINDOW_SERVICE)).thenReturn(windowManager);

        Compass compass = new Compass(mSensorManager, mContext);
        Location currentPosition = createLocation( 54.1943800f, 16.1722200f); // Koszalin
        Location destination = createLocation(52.520008f, 13.404954f); // Berlin
        compass.start();
        TestObserver<Double> destinationDirectionObserver = compass.getDestinationDirectionSubject().test();
        TestObserver<Double> northDirectionObserver = compass.getNorthDirectionSubject().test();

        //given (this sensor valeus should give bearing == 0
        compass.setCurrentPosition(currentPosition);
        compass.setDestination(destination);
        compass.onSensorChanged(createSensorEvent(new float[]{
                        0.2813187f,
                        -0.15322891f,
                        9.677363f
                },
                Sensor.TYPE_ACCELEROMETER));
        compass.onSensorChanged(createSensorEvent(new float[]{
                        0.18029928f,
                        -0.09419687f,
                        6.6110086f
                },
                Sensor.TYPE_MAGNETIC_FIELD));

        //then
        assertEquals(1, destinationDirectionObserver.values().size());
        assertEquals(1, northDirectionObserver.values().size());
        assertEquals(-134.2d, destinationDirectionObserver.values().get(0), 0.1);
        assertEquals(0, northDirectionObserver.values().get(0), 0.1);

        compass.stop();


    }

    private Location createLocation(float lat, float lng){
        Location loc = new Location("");
        loc.setLatitude(lat);
        loc.setLongitude(lng);
        return loc;
    }

    public SensorEvent createSensorEvent(float[] values, int sensorType) {
        SensorEvent sensorEvent = Mockito.mock(SensorEvent.class);
        Sensor sensor = Mockito.mock(Sensor.class);
        when(sensor.getType()).thenReturn(sensorType);
        try {
            Field valuesField = SensorEvent.class.getField("values");
            valuesField.setAccessible(true);
            try {
                valuesField.set(sensorEvent, values);
                sensorEvent.sensor = sensor;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return sensorEvent;
    }


}
