package com.jakubmateusiak.compassproject.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Jakub on 07.01.2018.
 */

@RunWith(JUnit4.class)
public class JavaTextUtilsTest {

    @Test
    public void isEmptyTest(){
        assertTrue(JavaTextUtils.isEmpty(null));
        assertTrue(JavaTextUtils.isEmpty(""));
        assertFalse(JavaTextUtils.isEmpty(" "));
        assertFalse(JavaTextUtils.isEmpty("test"));
    }
}
