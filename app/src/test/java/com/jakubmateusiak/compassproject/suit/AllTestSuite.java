package com.jakubmateusiak.compassproject.suit;

import com.jakubmateusiak.compassproject.features.compass.CompassPresenterTest;
import com.jakubmateusiak.compassproject.features.compass.CompassTest;
import com.jakubmateusiak.compassproject.utils.JavaTextUtilsTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Jakub on 05.01.2018.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CompassPresenterTest.class,
        CompassTest.class,
        JavaTextUtilsTest.class
})
public class AllTestSuite {
}
